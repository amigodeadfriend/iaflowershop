# Tasks for program
***

## Task 1

Design object model for given domain.
Please, demonstrate usage of:
- Classes (abstract classes - if applicable);
- Interfaces;
- Inheritance;
- Polymorphism;
- Encapsulation;
- Collections framework.

Each class, method and variable should have meaningful name and purpose.
You need to determine which classes are required to solve the task.
Inheritance should be used only when it makes sense.
Classes should be grouped into corresponding packages.
Work with console should be minimized (only required data for input, output correspond the info in task).
It is expected that you will choose certain domain from the list below, define classes hierarchy for that domain and implement the using OOP (inheritance or implementing interfaces).
Each class should have methods and fields defined by you.
Your program should create objects of different types in chosen domain and combine them into collection (use collection framework).
Next, will be required to process data of that collection by certain rules.


### Chosen domain - Flower Shop

Define flowers hierarchy.
Create several flower-objects.
Create a bouquet and determine its price.

## Task 2
For the object model implemented in task 1, it is needed to design custom exception classes and implement handling of possible exceptional situations.
Leave your explanation in commented blocks of your exception class.
Create at least 3 custom exceptions and use 5 built-in exceptions minimum.

***




