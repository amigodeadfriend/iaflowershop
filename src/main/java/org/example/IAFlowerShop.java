package org.example;

import org.example.bouquetmakers.DuoBouquetMaker;
import org.example.bouquetmakers.MonoBouquetMaker;
import org.example.bouquetmakers.TrioBouquetMaker;
import org.example.cassa.PricesMap;
import org.example.storagedata.AmountsMap;
import org.example.storagedata.FlowerType;

import java.util.Scanner;

public class IAFlowerShop {
    public static void main(String[] args) {
        try (Scanner scanner = new Scanner(System.in)) {
            PricesMap flowerPrices = new PricesMap();
            AmountsMap amountsMap = new AmountsMap();

            System.out.println("Welcome to flower shop!");
            System.out.println("Today we have these flowers: " + java.util.Arrays.asList(FlowerType.values()));
            System.out.println("Please choose the bouquet type - MONO / DUO / TRIO:");


            while (true) {

                try {
                    String typedType = scanner.nextLine();
                    switch (typedType) {
                        case "MONO" -> {
                            MonoBouquetMaker myMonoBouquetMaker = new MonoBouquetMaker(flowerPrices, amountsMap);
                            myMonoBouquetMaker.displayResult();
                            return;
                        }
                        case "DUO" -> {
                            DuoBouquetMaker myDuoBouquetMaker = new DuoBouquetMaker(flowerPrices, amountsMap);
                            myDuoBouquetMaker.displayResult();
                            return;
                        }
                        case "TRIO" -> {
                            TrioBouquetMaker myTrioBouquetMaker = new TrioBouquetMaker(flowerPrices, amountsMap);
                            myTrioBouquetMaker.displayResult();
                            return;
                        }

                        default -> throw new IllegalArgumentException("Invalid bouquet type. Please choose again.");
                    }

                } catch (IllegalArgumentException e) {
                    System.out.println(e.getMessage());
                }
            }
        }
    }
}