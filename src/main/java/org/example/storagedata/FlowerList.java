package org.example.storagedata;

import java.util.ArrayList;
import java.util.List;

public class FlowerList {

    private final List<FlowerType> flowerTypeList;

    public FlowerList() {
        flowerTypeList = new ArrayList<>();
        setFlowerList();
        setFlowers();
    }

    public void setFlowerList() {

        flowerTypeList.add(FlowerType.ROSE);
        flowerTypeList.add(FlowerType.LILY);
        flowerTypeList.add(FlowerType.TULIP);
        flowerTypeList.add(FlowerType.PEONY);
        flowerTypeList.add(FlowerType.ORCHID);
    }

    public void setFlowers() {

        flowerTypeList.add(FlowerType.valueOf(String.valueOf(FlowerType.ROSE)));
        flowerTypeList.add(FlowerType.valueOf(String.valueOf(FlowerType.LILY)));
        flowerTypeList.add(FlowerType.valueOf(String.valueOf(FlowerType.TULIP)));
        flowerTypeList.add(FlowerType.valueOf(String.valueOf(FlowerType.PEONY)));
        flowerTypeList.add(FlowerType.valueOf(String.valueOf(FlowerType.ORCHID)));
    }

    public String getFlowerOne() {
        return String.valueOf(FlowerType.ROSE);
    }

    public String getFlowerTwo() {
        return String.valueOf(FlowerType.LILY);
    }

    public String getFlowerThree() {
        return String.valueOf(FlowerType.TULIP);
    }

    public String getFlowerFour() {
        return String.valueOf(FlowerType.PEONY);
    }

    public String getFlowerFive() {
        return String.valueOf(FlowerType.ORCHID);
    }

}
