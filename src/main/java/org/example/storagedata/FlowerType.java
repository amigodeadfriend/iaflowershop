package org.example.storagedata;

public enum FlowerType {
    ROSE,
    LILY,
    TULIP,
    PEONY,
    ORCHID;
}
