package org.example.storagedata;

import java.util.HashMap;
import java.util.Map;

public class AmountsMap {
    FlowerList flowerList;

    private final Map<String, Integer> amounts = new HashMap<>();

    public AmountsMap() {

        flowerList = new FlowerList();
        setAmountsMap();
    }

    public void setAmountsMap() {

        String flowerOne = flowerList.getFlowerOne();
        int flowerOneAmount = 20;
        amounts.put(flowerOne, flowerOneAmount);

        String flowerTwo = flowerList.getFlowerTwo();
        int flowerTwoAmount = 20;
        amounts.put(flowerTwo, flowerTwoAmount);

        String flowerThree = flowerList.getFlowerThree();
        int flowerThreeAmount = 20;
        amounts.put(flowerThree, flowerThreeAmount);

        String flowerFour = flowerList.getFlowerFour();
        int flowerFourAmount = 20;
        amounts.put(flowerFour, flowerFourAmount);

        String flowerFive = flowerList.getFlowerFive();
        int flowerFiveAmount = 20;
        amounts.put(flowerFive, flowerFiveAmount);

    }

    public Map<String, Integer> getAmountsMap() {
        return amounts;
    }
}
