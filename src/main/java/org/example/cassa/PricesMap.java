package org.example.cassa;

import org.example.storagedata.FlowerList;

import java.util.HashMap;
import java.util.Map;

public class PricesMap {

    FlowerList flowerList;

    private final Map<String, Integer> prices = new HashMap<>();


    public PricesMap() {
        flowerList = new FlowerList();
        setPricesMap();
    }

    private void setPricesMap() {

        String flowerOne = flowerList.getFlowerOne();
        int flowerOnePrice = 100;
        prices.put(flowerOne, flowerOnePrice);

        String flowerTwo = flowerList.getFlowerTwo();
        int flowerTwoPrice = 70;
        prices.put(flowerTwo, flowerTwoPrice);

        String flowerThree = flowerList.getFlowerThree();
        int flowerThreePrice = 50;
        prices.put(flowerThree, flowerThreePrice);

        String flowerFour = flowerList.getFlowerFour();
        int flowerFourPrice = 100;
        prices.put(flowerFour, flowerFourPrice);

        String flowerFive = flowerList.getFlowerFive();
        int flowerFivePrice = 200;
        prices.put(flowerFive, flowerFivePrice);

    }

    public Map<String, Integer> getPrices() {
        return prices;
    }
}
