package org.example.bouquetmakers;

import org.example.cassa.PricesMap;
import org.example.storagedata.AmountsMap;
import org.example.storagedata.FlowerType;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.Scanner;
import java.util.logging.Level;
import java.util.logging.Logger;

public class TrioBouquetMaker implements BouquetMaker {

    private final PricesMap pricesMap;
    private final AmountsMap amountsMap;
    private String flower1;
    private String flower2;
    private String flower3;
    private String flowers;
    private int amount1;
    private int amount2;
    private int amount3;
    private int bouquetSize;
    private int flowerPrice1;
    private int flowerPrice2;
    private int flowerPrice3;
    private int bouquetPrice;
    private List<String> bouquet;
    Logger logger = Logger.getLogger(TrioBouquetMaker.class.getName());

    public TrioBouquetMaker(PricesMap flowerPrices, AmountsMap flowerAmounts) {
        pricesMap = flowerPrices;
        amountsMap = flowerAmounts;
        chooseFlowers();
        defineBouquetSize();
        makeBouquet();
        defineBouquetPrice();
    }

    @Override
    public void chooseFlowers() {

        while (true) {

            System.out.println("Please choose the flower1 type -" + java.util.Arrays.asList(FlowerType.values()));
            Scanner scanner1 = new Scanner(System.in);
            flower1 = String.valueOf(FlowerType.valueOf(scanner1.nextLine()));

            System.out.println("Please choose the flower2 type -" + java.util.Arrays.asList(FlowerType.values()));
            Scanner scanner2 = new Scanner(System.in);
            flower2 = String.valueOf(FlowerType.valueOf(scanner2.nextLine()));

            System.out.println("Please choose the flower2 type -" + java.util.Arrays.asList(FlowerType.values()));
            Scanner scanner3 = new Scanner(System.in);
            flower3 = String.valueOf(FlowerType.valueOf(scanner3.nextLine()));
            try {
                if (Objects.equals(flower1, flower2) || Objects.equals(flower2, flower3) || Objects.equals(flower1, flower3)) {
                    throw new IllegalArgumentException("For trio bouquet chosen flowers should be different");
                } else {
                    break;
                }
            } catch (IllegalArgumentException e) {
                logger.log(Level.WARNING, e.getMessage());
            }
        }
        flowers = flower1 + ", " + flower2 + " and " + flower3;
    }

    @Override
    public void defineBouquetSize() {

        while (true) {
            System.out.println("Please choose the amount of flower1:");
            Scanner scanner1 = new Scanner(System.in);
            int flowerAmount1 = amountsMap.getAmountsMap().get(flower1);
            amount1 = scanner1.nextInt();

            System.out.println("Please choose the amount of flower2:");
            Scanner scanner2 = new Scanner(System.in);
            int flowerAmount2 = amountsMap.getAmountsMap().get(flower2);
            amount2 = scanner2.nextInt();

            System.out.println("Please choose the amount of flower3:");
            Scanner scanner3 = new Scanner(System.in);
            int flowerAmount3 = amountsMap.getAmountsMap().get(flower3);
            amount3 = scanner3.nextInt();

            try {
                if (amount1 > flowerAmount1 || amount2 > flowerAmount2 || amount3 > flowerAmount3) {
                    throw new IllegalArgumentException("Sorry, we don't have such amount of flowers");
                } else {
                    break;
                }
            } catch (IllegalArgumentException e) {
                logger.log(Level.WARNING, e.getMessage());
            }
        }
        bouquetSize = amount1 + amount2 + amount3;
    }

    @Override

    public void makeBouquet() {
        bouquet = new ArrayList<>();
        for (int i = 0; i < bouquetSize; i++) {
            if (i < amount1) {
                bouquet.add(flower1);
            }
            if (i < amount2) {
                bouquet.add(flower2);
            }
            if (i < amount3) {
                bouquet.add(flower3);
            }
        }
    }


    @Override
    public void defineBouquetPrice() {

        flowerPrice1 = pricesMap.getPrices().get(flower1);
        flowerPrice2 = pricesMap.getPrices().get(flower2);
        flowerPrice3 = pricesMap.getPrices().get(flower3);
        bouquetPrice = flowerPrice1 * amount1 + flowerPrice2 * amount2 + flowerPrice3 * amount3;

    }

    @Override
    public void displayResult() {

        String amt = "The amount of ";
        String prc = "The price of ";
        String messageOfBouquetType = "You chose a trio bouquet";
        String messageOfBouquetFlowers = "Chosen flowers - " + flowers;
        String messageOfBouquetFlowerOne = amt + flower1 + " - " + amount1;
        String messageOfBouquetFlowerTwo = amt + flower2 + " - " + amount2;
        String messageOfBouquetFlowerThree = amt + flower3 + " - " + amount3;
        String messageOfBouquetSize = "Bouquet size - " + bouquetSize;
        String messageOfBouquetView = "This is the bouquet - " + bouquet;
        String messageOfBouquetFlowerOnePrice = prc + flower1 + " - " + flowerPrice1;
        String messageOfBouquetFlowerTwoPrice = prc + flower2 + " - " + flowerPrice2;
        String messageOfBouquetFlowerThreePrice = prc + flower3 + " - " + flowerPrice3;
        String messageOfBouquetPrice = prc + "the bouquet - " + bouquetPrice;

        String messageAllInfo = messageOfBouquetType + "\n"
                + messageOfBouquetFlowers + "\n"
                + messageOfBouquetFlowerOne + "\n" + messageOfBouquetFlowerTwo + "\n" + messageOfBouquetFlowerThree + "\n"
                + messageOfBouquetSize + "\n"
                + messageOfBouquetView + "\n"
                + messageOfBouquetFlowerOnePrice + "\n" + messageOfBouquetFlowerTwoPrice + "\n" + messageOfBouquetFlowerThreePrice + "\n"
                + messageOfBouquetPrice + "\n" + "\n"
                + "Thank you for your order, hope to see you soon!";

        logger.log(Level.INFO, messageAllInfo);
    }
}
