package org.example.bouquetmakers;


import org.example.cassa.PricesMap;
import org.example.storagedata.AmountsMap;
import org.example.storagedata.FlowerType;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Scanner;
import java.util.logging.Level;
import java.util.logging.Logger;

public class MonoBouquetMaker implements BouquetMaker {
    private final PricesMap pricesMap;
    private final AmountsMap amountsMap;
    private String flower;
    private int amount;
    private int flowerPrice;
    private int bouquetPrice;
    private List<String> bouquet;
    Logger logger = Logger.getLogger(MonoBouquetMaker.class.getName());

    public MonoBouquetMaker(PricesMap flowerPrices, AmountsMap flowerAmounts) {

        pricesMap = flowerPrices;
        amountsMap = flowerAmounts;
        chooseFlowers();
        defineBouquetSize();
        makeBouquet();
        defineBouquetPrice();
    }

    @Override
    public void chooseFlowers() {

        System.out.println("Please choose the flower type -" + Arrays.asList(FlowerType.values()));
        Scanner scanner = new Scanner(System.in);
        flower = String.valueOf(FlowerType.valueOf(scanner.nextLine()));

    }


    @Override
    public void defineBouquetSize() {

        while (true) {
            System.out.println("Please choose the amount of flowers:");
            int flowerAmount = amountsMap.getAmountsMap().get(flower);
            Scanner scanner = new Scanner(System.in);
            amount = scanner.nextInt();

            try {
                if (amount > flowerAmount) {
                    throw new IllegalArgumentException("Sorry, we don't have such amount of flowers");
                } else {
                    break;
                }
            } catch (IllegalArgumentException e) {
                logger.log(Level.WARNING, e.getMessage());
            }
        }
    }


    @Override
    public void makeBouquet() {

        bouquet = new ArrayList<>();
        for (int i = 0; i < amount; i++) {
            bouquet.add(flower);
        }

    }


    @Override
    public void defineBouquetPrice() {

        flowerPrice = pricesMap.getPrices().get(flower);
        bouquetPrice = flowerPrice * amount;

    }

    @Override
    public void displayResult() {

        String messageOfBouquetType = "You chose a mono bouquet";
        String messageOfBouquetFlower = "Chosen flower - " + flower;
        String messageOfBouquetSize = "Bouquet size - " + amount;
        String messageOfBouquetView = "This is the bouquet - " + bouquet;
        String messageOfBouquetFlowerPrice = "The price of one flower - " + flowerPrice;
        String messageOfBouquetPrice = "The price of the bouquet - " + bouquetPrice;

        String messageAllInfo = messageOfBouquetType + "\n"
                + messageOfBouquetFlower + "\n"
                + messageOfBouquetSize + "\n"
                + messageOfBouquetView + "\n"
                + messageOfBouquetFlowerPrice + "\n"
                + messageOfBouquetPrice + "\n" + "\n"
                + "Thank you for your order, hope to see you soon!";

        logger.log(Level.INFO, messageAllInfo);
    }
}
