package org.example.bouquetmakers;

public interface BouquetMaker {
    void chooseFlowers();

    void defineBouquetSize();

    void makeBouquet();

    void defineBouquetPrice();

    void displayResult();
}
